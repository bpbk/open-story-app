/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var $document;
var map;
var mapmarkers = [];
var myID = 23;
var startStoryMap;
var currentLoc = [];

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);

    },
    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
	    this.myUserInfo = {'username':'brandon', 'firstname':'brandon', 'lastname':'phillips', 'id':myID, 'description':''};
	    $document = this;
	    this.getAllGenres();
        $document.receivedEvent('deviceready');
        $('.back_button').click(function(){
	       $('#story_container').removeClass('active');
        });
        $('.back_one').on('click', function(){
	    	$(this).closest('.overlay_view').removeClass('active');
        });
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        this.menuFunctions();
        //push_notifications();
        //$document.pushNotifications();

/*
        navigator.geolocation.getCurrentPosition(onSuccess, onError, { timeout: 30000 });
		function onSuccess(position) {
		 var lat=position.coords.latitude;
		 var lang=position.coords.longitude;
		}
		function onError(error) { alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n'); }
*/

        //listeningElement.setAttribute('style', 'display:none;');
        //receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
		//localStorage.setItem("myID", myID);
        //localStorage.removeItem('myID');
        if(localStorage.getItem("myID")){
	        $document.setupMaps();
        }else{
	        $document.reglog = 'register';
	        $('#onboarding_screen').addClass('active');
	        $('#reglog_submit_userinfo').on('click', function(){
		        console.log($document.reglog);
		        $document.submitUserInfo();
	        });
	        $('.reglog_toggle').on('click', function(){
		        switch($document.reglog){
			        case 'login':
			        	$document.reglog = 'register';
				        $('#reglog_username').parent().slideDown(200);
				        setTimeout(function(){
					        $('.trouble_logging_in').slideUp();
					        $('#reglog_submit_userinfo').text('sign up');
					        $('.reglog_toggle').text('Have an account? Log in');
				        }, 300);
			        break;
			        case 'register':
			        	$document.reglog = 'login';
				        $('#reglog_username').parent().slideUp(200);
				        setTimeout(function(){
					        $('.trouble_logging_in').slideDown();
					        $('#reglog_submit_userinfo').text('log in');
					        $('.reglog_toggle').text('register for an account');
				        }, 300);
			        break;
		        }
	        });
        }

/*
        navigator.geolocation.getCurrentPosition(handle_geolocation_query);
		function handle_geolocation_query(position){
			console.log('the POSITION IS '+position);
            alert('Lat: ' + position.coords.latitude + ' ' +
                  'Lon: ' + position.coords.longitude);
        }
*/
    },
    /*----------------------------------------------*\
	    Init push notifications *Untested
	\*----------------------------------------------*/
    pushNotifications: function(){
	    window.GcmPushPlugin.register(this.successHandler, this.errorHandler, {
          	"badge":"true",
		  	"sound":"true",
		  	"alert":"true",
		  	"usesGCM":true,
		  	"sandbox":true,
		  	"jsCallback":"onNotification"
        });
    },
    successHandler: function(result) {
	  	alert("Token: " + result.gcm);
	},
	errorHandler: function(error) {
		alert("Error: " + error);
	},
    /*----------------------------------------------*\
	    Init map based on obj identifier and div id
	\*----------------------------------------------*/
    fireMap: function(mapObj, mapID){
	    var options = {
	//		camera: {
	//	    	latLng: [{'lat':1.23043, 'lng': 0.2342114}] // an array of LatLng objects
	//	  	}
			styles: mapStyles,
			disableDefaultUI: true,
		};
		$document[mapObj] = new google.maps.Map(document.getElementById(mapID), options);
    },
    /*---------------------------------------*\
	    Get all genres at the start
	\*---------------------------------------*/
    getAllGenres: function(){
	    $.get( "http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/getgenres", function( data ) {
        	$document.allGenres = data;
        	$document.showRegistrationGenres();
    	});
    },
    /*----------------------------------------------------------*\
	    Setup menu structure. This should be cleaned up/DRYer
	\*----------------------------------------------------------*/
    menuFunctions: function(){
	  	$('.menu_button').on('click', function(){
        var navClicked = $(this).attr('btnfunc');
        if($document.currentScreen != navClicked){
          $document.currentScreen = navClicked;
          switch(navClicked){ // Switch based on btnfunc attribute
            case 'stories':
              $('#home_screen').removeClass('left').removeClass('right').removeClass('active');
              $('.overlay_view.active').removeClass('active');
            break;
            case 'feed':
            break;
            case 'start-story':
              $('#start_story_view').addClass('active');
              $document.startStory();
            break;
            case 'nearby':
              $('#nearby_stories_view').addClass('active');
              $('#home_screen').addClass('right');
              $document.getNearbyStories();
            break;
            case 'daily':
              $('.overlay_view').removeClass('active');
              $('#daily_stories_view').addClass('active');
              $('#home_screen').addClass('left');
              $document.getDailyPrompt();
            break;
            default:
            break;
          }
        }
		});
		$('#edit_profile_button').on('click', function(){
			if(!$('#edit_profile_view').hasClass('active')){
				$('#edit_profile_view').addClass('active');
				$('#home_screen').addClass('left');
			}
		});
		//startStoryFunctions();
    },
    /*----------------------------------------------*\
	    Start a new story
	  \*----------------------------------------------*/
    startStory: function(){
	    // First check for draft.
	    $('#footer_menu').addClass('hidden');
	    var isDraft = false;
	    if(isDraft){

	    }else{
		    $document.newStoryObj = new Object();;
		    $document.genreArray = [];
		    $('#start_story_screen').html('<div id="story_header" class="editable"><div class="story_header_placeholder"></div><div class="story_header_content"><div class="story_header_info"><div class="add_story_header_image"></div><div class="story_header_info_content"><h1 id="story_title_input" class="editable" contenteditable="true" placeholder="Story Title"></h1><input type="submit" value="genre" id="choose_genre_button"/></div></div></div></div></div><div id="start_story_content"><div id="first_chapter_title_input" contenteditable="true" class="editable" placeholder="chapter title"></div><div contenteditable="true" class="editable" id="story_text_input" placeholder="write your story"></div>');
		    // This might move to the step after you write the story. Where it is now may be missed by the user and then we'd have to have a second step anyways. Also it gives people time to figure out what genre their story turns out to be.
		    $('#choose_genre_button').off('click').on('click', function(){
			    $document.showNewStoryGenres();
			});
			$('#place_story_locations').off('click').on('click', function(){
				$document.placeNewStoryLocations();
			});
			$('#submit_story').on('click', function(){
				var storyLocs = [];
		 		$.each($document.startStoryLocs, function(l, loc){
		 			storyLocs.push([loc.lat(), loc.lng()]);
				});
				$document.newStoryObj.genres = JSON.stringify($document.genreArray);
				$document.newStoryObj.slocs = JSON.stringify(storyLocs);
				$document.submitStory();
			});
	    }
	},
	showNewStoryGenres: function() {
		var storyTitle = document.getElementById("story_title_input").innerHTML;
		var chapterTitle = document.getElementById("first_chapter_title_input").innerHTML;
	    var storyText = document.getElementById("story_text_input").innerHTML;
		if(storyTitle == ""){
	        //navigator.notification.alert("your story needs a title", null, "title missing", "OK");
	        return;
	    }
	    if(storyText == ""){
	        //navigator.notification.alert("write your story", null, "story", "OK");
	        return;
	    }
	    if(chapterTitle == ""){
	        //navigator.notification.alert("write your story", null, "story", "OK");
	        return;
	    }
    	$document.newStoryObj.stitle = storyTitle;
    	$document.newStoryObj.ctitle = chapterTitle;
    	$document.newStoryObj.ctext = storyText;
    	$document.newStoryObj.user = $document.myUserInfo.id;
    	console.log($document.newStoryObj);
		var genreul = document.createElement('ul');
        $('#story_genres_list').html(genreul);
        $('#story_genres_screen').addClass('active');
        $('#start_story_screen').addClass('hidden');
		$.each($document.allGenres, function(i, genre){
			var genreli = document.createElement('li');
				genreli.innerHTML = '<h3>'+genre.name+'</h3>';
				genreli.onclick = function () {
					var selectedGenre = $.inArray(genre.slug, $document.genreArray);
					if (selectedGenre == -1) {
					  $document.genreArray.push(genre.slug);
					} else {
					  $document.genreArray.splice(selectedGenre, 1);
					}
				};
	        $('#story_genres_list ul').append(genreli);
		});
	},
	placeNewStoryLocations:function(){
		$document.startStoryLocs = [];
		if(!$document.newStoryMap){
		    $document.fireMap('newStoryMap', 'place_locations_map_content');
	    }
	    if($document.mapmarkers.length > 0){
			$document.clearMap();
		}
		$('#place_locations_map').addClass('active');
		$('#place_locations_map_content').html('<div id="locations_map"></div>');

		var position = new google.maps.LatLng($document.currentLoc[0], $document.currentLoc[1]);
		$document.startStoryLocs.push(position); // Push current location to list of locations
		var options = {
			camera: {
		    	latLng: position // an array of LatLng objects
		  	},
			styles: mapStyles,
			disableDefaultUI: true,
		};
		$document.startStoryMap = new google.maps.Map(document.getElementById("locations_map"), options);
		var bounds = new google.maps.LatLngBounds();

		bounds.extend(position); // add position to bounds to show all pins
		marker = new google.maps.Marker({
	        position: position,
	        map: $document.startStoryMap,
	    });
	    $document.startStoryMap.fitBounds(bounds); //Set bounds of the map to show all markers
		google.maps.event.addListener($document.startStoryMap, 'click', function(event) {
		   $document.placeMarker(event.latLng);
		   $document.startStoryLocs.push(event.latLng); // Add position of the marker that the person dropped.
		});
		var boundsListener = google.maps.event.addListener(($document.startStoryMap), 'bounds_changed', function(event) {
           this.setZoom(16);
           google.maps.event.removeListener(boundsListener);
        });
	},
	placeMarker: function(location){
		var storymarker = new google.maps.Marker({
	        position: location,
	        draggable: true,
	        map: $document.startStoryMap
	    });
	    var dragStart = google.maps.event.addListener(storymarker, 'dragstart', function(evt){
			$document.startStoryLocs = $.grep($document.startStoryLocs, function(e){
			     return e != evt.latLng; // Listen for when the drag starts. Find that pin in the array and remove it because its going to change.
			});
		});
	    var dragEnd = google.maps.event.addListener(storymarker, 'dragend', function(evt){
		    $document.startStoryLocs.push(evt.latLng); // Listen for when the drag stops and add it to the array of story locations
		    console.log(evt.latLng.lat());
		});
	},
	submitStory: function() {
		$.ajax({
	        url: 'http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/submitstory/',
	        method: 'GET',
	        data: {'story': JSON.stringify($document.newStoryObj)},
	        crossDomain: true,
	        dataType: 'json',
	        contentType: 'application/json',
	        //beforeSend: function ( xhr ) {
	        //    xhr.setRequestHeader( 'Authorization', 'Basic username:password' );
	        //},
	        success: function( data ) {
	            console.log( data );
	        },
	        error: function( error ) {
	            console.log( error );
	        }
	    });
	},
	setupMaps:function() {
		$document.mapmarkers = [];
	    $document.currentLoc = [40.6744073, -73.943335];
	    $document.storyIcon = {
	        url: 'img/storyIcon.svg',
	        size: new google.maps.Size(25, 29),
	        origin: new google.maps.Point(0, 0),
	        anchor: new google.maps.Point(25/2, 29/2)
		};
		$document.getMyStories();
	},
    /*---------------------------------------*\
	    Retrieve all of my stories from DB
	\*---------------------------------------*/
    getMyStories: function() {
	    myID = localStorage.getItem("myID");
	    $('#home_screen').addClass('active');
		$.get( "http://fullmetalworkshop.com/openstoryapp/wp-admin/admin-ajax.php?action=user_stories&user="+myID, function( data ) {
		    //var posts_array = JSON.parse(data);
		    var storiesData;
		    if(data){
		        var storiesData = JSON.parse(data);
		        if($('#onboarding_screen').hasClass('active')){
			        $('#onboarding_screen').removeClass('active');
		        }
		    }
		    $document.placeMyStories(storiesData);
		});
    },
    /*---------------------------------------*\
	    Place stories on homepage
	\*---------------------------------------*/
    placeMyStories: function(storiesData) {
	    console.log(storiesData);
	    var storyTypes = {
	    'iwrote' :
	    	{
	    		'cta': 'Start a new story',
				'title': 'Stories'
	    	},
	    'icontributed' :
	    	{
	    		'cta': 'Find a nearby story',
	    		'title': 'Contributions'
	    	},
	    'daily' : {
		    	'cta' : 'Write a daily story',
		    	'title' : 'Daily'
	    	}
	    };
	    $('#my_stories_lists').empty();
	    $.each(storyTypes, function(key, stype){
		    var storyul = document.createElement('ul');
			    storyul.className = key+' story_list';
			$('#my_stories_lists').append('<div id="list_'+key+'" class="story_list_header"><h2>'+stype.title+'</h2></div>');
		    $('#my_stories_lists').append(storyul);
		    if($.isArray(storiesData[key])){
			    $('.'+key).addClass('has_stories');
			    $.each(storiesData[key], function(i, story){
					var storyli = document.createElement('li');
						storyli.innerHTML = '<div class="story_list_item_container"><div class="story_list_item_content"><div class="story_background bg_centered" style="background-image:url('+story.image+');"></div><div class="story_list_info"><div class="bottom_gradient"></div><h3>'+story.title+'</h3><p>'+story.genres[0].name+'</p></div></div></div>';
						storyli.onclick = function () {
							$document.currentStory = story;
							$document.setupMyStoryView(key); // Setup header and then determine whether there are multiple locations or not
							if(story.locs && story.locs.length > 1){
								$('#my_stories_map').show();
							}else{
								if(story.locs && story.locs.length == 1){
									$document.getStory(story.locs[0].id);
								}else{

								}
								$('#my_stories_map').hide();
							}
						};
			        $('#my_stories_lists ul.'+key).append(storyli);
				});
			}
			var storyli = document.createElement('li');
				storyli.className = 'call_to_action';
				storyli.innerHTML = '<div class="story_list_item_container"><div class="story_list_item_content">'+stype.cta+'</div></div>';
				storyli.onclick = function () {
					// Start new story
				};
			    $('#my_stories_lists ul.'+key).append(storyli);
	    });
/*
		$.each(storiesData, function(key, storyList){
		    if($.isArray(storyList)){
				$.each(storyList, function(i, story){
					var storyli = document.createElement('li');
						storyli.innerHTML = '<div class="story_list_item_container"><div class="story_list_item_content"><div class="story_background bg_centered" style="background-image:url('+story.image+');"></div><div class="story_list_info"><div class="bottom_gradient"></div><h3>'+story.title+'</h3><p>'+story.genres[0].name+'</p></div></div></div>';
						storyli.onclick = function () {
							$document.currentStory = story;
							$document.setupMyStoryView(key); // Setup header and then determine whether there are multiple locations or not
							if(story.locs && story.locs.length > 1){
								$('#my_stories_map').show();
							}else{
								if(story.locs && story.locs.length == 1){
									$document.getStory(story.locs[0].id);
								}else{

								}
								$('#my_stories_map').hide();
							}
						};
			        $('#my_stories_lists ul.'+key).append(storyli);
				});
				var storyli = document.createElement('li');
					storyli.className = 'call_to_action';
					storyli.innerHTML = '<div class="story_list_item_container"><div class="story_list_item_content">'+callToAction+'</div></div>';
					storyli.onclick = function () {
						// Start new story
					};
				    $('#my_stories_lists ul.'+key).append(storyli);
			}
		});
*/
    },
    /*---------------------------------------*\
	    Place header image, title and map
	\*---------------------------------------*/
    setupMyStoryView: function(storytype){
	    console.log(storytype);
	    var story = $document.currentStory;
	    var storyTitle = story.title;
	    if(storytype == 'daily'){
		    storyTitle = story.prompt.prompt_text;
	    }
	    $('#my_story_header').html('<div class="story_header_content"><div id="story_header_image" class="bg_centered" style="background-image:url('+story.image+')"></div><div class="story_header_info"><div class="bottom_gradient"></div><div class="story_header_info_content"><h1>'+storyTitle+'</h1></div></div></div>');
	    $('#story_container, #home_screen').addClass('active');

	    switch(storytype){
		    case 'iwrote':

		    break;
		    case 'icontributed':
		    	console.log(story.author);
		    	$('.story_header_info').append('<div class="author_image bg_centered" style="background-image:url('+story.author.image+')"></div>');
		    	$('.story_header_info_content').append('<h5>'+story.author.username+'</h5>');
		    break;
		    case 'daily':
		    	console.log(story);
		    break;
	    }
	    $('#story_container_content ul').empty();

	    if(storytype != 'daily'){
		    if(!$document.myStoriesMap){
			    $document.fireMap('myStoriesMap', 'my_stories_map');
		    }
		    $document.placeMyStoryLocations();
	    }
    },
    /*---------------------------------------*\
	    Place pins
	\*---------------------------------------*/
    placeMyStoryLocations: function(){
	    if($document.mapmarkers.length > 0){
			$document.clearMap();
		}
		var markers = $document.currentStory.locs;
		var infoWindow = new google.maps.InfoWindow(), marker, i;
		var bounds = new google.maps.LatLngBounds();
	    $.each(markers, function(l, loc){
	        var position = new google.maps.LatLng(loc.lat, loc.lng);
	        var locId = loc.id;
	        bounds.extend(position);
	        marker = new google.maps.Marker({
		        position: position,
		        map: $document.myStoriesMap,
		        icon: $document.storyIcon
		    });
		    $document.mapmarkers.push(marker);
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
	            return function() {
	                //infoWindow.setContent(storyDetails);
	                //infoWindow.open(map, marker);
	                $document.getStory(locId);
	            }
		    })(marker, i));

	    });
	    $document.myStoriesMap.fitBounds(bounds);

			var boundsListener = google.maps.event.addListener(($document.myStoriesMap), 'bounds_changed', function(event) {
               	this.setZoom(16);
                google.maps.event.removeListener(boundsListener);
            });

    },
    /*---------------------------------------*\
	    Get chapters by location
	\*---------------------------------------*/
    getStory: function(locId){
	    var story = $document.currentStory;
	    $.get( "http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/getchapters/"+locId, function( data ) {

/*
			if($document.myUserInfo){
				var authorLink = document.createElement('h2');
					authorLink.innerHTML = $document.myUserInfo.username;
					authorLink.onclick = function(){
						// Go to author page.
					}
				$('#story_header_info').append(authorLink);
			}
*/
	        $.each( data, function( i, chap ) {
		        console.log(chap);
		        var chapterli = document.createElement('li');
		        	chapterli.className = 'text_box';
					chapterli.innerHTML = '<h3>'+chap.title+'</h3><h5 authid="'+chap.authorid+'">'+chap.author+'</h5>'+chap.content;
					$('#story_container_content ul').append(chapterli);
					$('#my_stories_map').slideUp();
	        });
	    });
    },
    /*----------------------------------------------*\
	    Find daily prompt
	\*----------------------------------------------*/
    getDailyPrompt: function(){
	    // The return on this needs to be updated based on time of day
	    $.get( "http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/getdaily/"+$document.myUserInfo.id, function( data ) {
			var dailyObj = JSON.parse(data);
			$('#daily_stories_view').html('<div id="story_header" class="editable"><div class="story_header_placeholder"></div><div class="story_header_content"><div class="story_header_info"><div class="add_story_header_image"></div><div class="story_header_info_content"><h1 id="story_title_input">'+dailyObj.prompt.text+'</h1></div></div></div></div><div id="daily_story"></div>');
			var dailyTitle = document.createElement("input");
				dailyTitle.className = 'daily_title';
				dailyTitle.setAttribute('type', 'text');
			var dailyText = document.createElement("div");
				dailyText.className = 'daily_text text_box';
				dailyText.setAttribute('contenteditable', true);
				$document.formatPTags(dailyText);
			var submitDaily = document.createElement("input");
				submitDaily.className = 'submit_daily';
				submitDaily.setAttribute('type', 'submit');
			if(dailyObj.chapter){
				dailyTitle.setAttribute('value', dailyObj.chapter.title);
				submitDaily.setAttribute('value', 'publish');
				submitDaily.setAttribute('id', dailyObj.chapter.id);
				submitDaily.setAttribute('status', dailyObj.chapter.status);
				dailyText.innerHTML = dailyObj.chapter.text;
			}else{
				submitDaily.setAttribute('value', 'submit');
				submitDaily.setAttribute('status', 'new');
			}
			submitDaily.addEventListener("click", function(e) {
			    alert('something');
			}, false);
			$('#daily_story').append(dailyTitle,dailyText,submitDaily);
		});
    },
    /*-------------------------------------------------------------*\
	    Get stories by location. Send location -> process in PHP
	\*-------------------------------------------------------------*/
    getNearbyStories: function(){
	    console.log($document.currentLoc);
		$.ajax({
	        url: 'http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/getnearbylocations/'+myID,
	        method: 'GET',
	        data:{
		        'lat' : $document.currentLoc[0],
		        'lng' : $document.currentLoc[1],
		        'dist' : .8,
	        },
	        dataType: 'json',
	        success: function( data ) {
		        var storiesData = JSON.parse(data);
		        console.log(storiesData);
	            if(storiesData.length > 0){
			        $document.placeNearbyLocations(storiesData);
		        }
	        },
	        error: function( error ) {
	            console.log( error );
	        }
	    });
	},
	/*---------------------------------------*\
	    Place location pins
	\*---------------------------------------*/
	placeNearbyLocations: function(data){

	    if(!$document.nearbyStoriesMap){
			$document.fireMap('nearbyStoriesMap', 'nearby_stories_map');
			var nearbystoriesul = document.createElement('ul');
				nearbystoriesul.className = 'story_list';
	        $('#nearby_stories_list').append(nearbystoriesul);
		}
		if($document.mapmarkers.length > 0){
			$document.clearMap();
			$('#nearby_stories_list ul').empty();
		}

		var bounds = new google.maps.LatLngBounds();

	    $.each(data, function(s, story){
		    var storyPos = new google.maps.LatLng(story.loc[0], story.loc[1]);

	        marker = new google.maps.Marker({
		        position: storyPos,
		        map: $document.nearbyStoriesMap,
		        icon: $document.storyIcon,
		    });
		    $document.mapmarkers.push(marker);
		    bounds.extend(storyPos);
		    var nearbystoryli = document.createElement('li');
		    	nearbystoryli.className = 'full_width_story';
				nearbystoryli.innerHTML = '<div class="story_list_item_container"><div class="story_list_item_content"><div class="story_background bg_centered" style="background-image:url('+story.image+');"></div><div class="story_list_info"><div class="bottom_gradient"></div><div class="story_list_info_content"><h3>'+story.title+'</h3><p>'+story.genres[0].name+'</p></div><div class="author_image bg_centered" style="background-image:url('+story.author.image+')"></div></div></div></div>';
				nearbystoryli.onclick = function () {
	            	// Center marker and show story with chapters
	            	$document.currentStory = story;
	                $document.viewNearbyStory();
				};
	        $('#nearby_stories_list ul').append(nearbystoryli);
		    google.maps.event.addListener(marker, 'click', (function(marker, s) {
	            return function() {
		            $document.currentStory = story;
	                $document.viewNearbyStory();
	            }
		    })(marker, s));
	    });
	    $document.nearbyStoriesMap.fitBounds(bounds); //Set bounds of the map to show all markers
		var boundsListener = google.maps.event.addListener(($document.nearbyStoriesMap), 'bounds_changed', function(event) {
	        this.setZoom(15);
	        google.maps.event.removeListener(boundsListener);
	    });
	},
	/*---------------------------------------*\
	    View nearby story (make DRY)
	\*---------------------------------------*/
	viewNearbyStory: function(){
		var story = $document.currentStory;
		$('#story_header').html('<div class="story_header_content"><div id="story_header_image" class="bg_centered" style="background-image:url('+story.image+')"></div><div class="story_header_info"><div class="bottom_gradient"></div><div class="story_header_info_content"><h1>'+story.title+'</h1></div><div class="author_image bg_centered" style="background-image:url('+story.author.image+')"></div></div></div>');
		$.get( "http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/getchapters/"+story.id, function( data ) {
			$('#nearby_container_content').html('<h1>'+story.title+'</h1><div id="chapter_list"><ul></ul></div>');
	        $.each( data, function( i, chap ) {
		        var chapterli = document.createElement('li');
		        	chapterli.className = 'text_box';
					chapterli.innerHTML = '<h3>'+chap.title+'</h3><h5 authid="'+chap.authorid+'">'+chap.author+'</h5>'+chap.content;
				$('#nearby_container_content ul').append(chapterli);
				$('#nearby_container, #home_screen').addClass('active');
	        });
	        $('#nearby_container_content').append('<form id="add_chapter_form"><input id="add_chapter_title" placeholder="new chapter title" /><textarea id="add_chapter_text" placeholder="chapter text"></textarea></form>');
	        var submitChapter = document.createElement('input');
	        	submitChapter.setAttribute('type', 'submit');
	        	submitChapter.setAttribute('value', 'submit chapter');
	        	$('#add_chapter_form').append(submitChapter);
	        	submitChapter.onclick = function(){
		        	var chapNum = data.length + 1;
		        	var chapTitle = document.getElementById("add_chapter_title").value;
		        	var chapText = document.getElementById("add_chapter_text").value;
		        	var locid = story.id;
		        	var genreIds = [];
		        	$.each(story.genres, function(g, genre){
			        	genreIds.push(genre.term_id);
		        	});
		        	if(chapTitle == ""){
				        //navigator.notification.alert("Please enter username", null, "Username Missing", "OK");
				        return;
				    }
				    if(chapText == ""){
				        //navigator.notification.alert("Please enter password", null, "Password Missing", "OK");
				        return;
				    }
				    console.log(chapNum, chapTitle, chapText, locid, genreIds);
				    $.ajax({
				        url: 'http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/addchapter/'+locid+'/?auth='+myID+'&ctitle='+chapTitle+'&ctext='+chapText+'&order='+chapNum+'&genres='+genreIds,
				        method: 'GET',
				        crossDomain: true,
				        dataType: 'json',
				        contentType: 'application/json',
				        success: function( data ) {
				            console.log(data);
				        },
				        error: function( error ) {
				            console.log( error );
				        }
				    });
				    return false;
	        	}
	    });
    },
    /*---------------------------------------*\
	    Clear map all pins
	\*---------------------------------------*/
    clearMap: function(){
	  	for(i=0; i< $document.mapmarkers.length; i++){
	        $document.mapmarkers[i].setMap(null);
	    }
	    $document.mapmarkers = [];
    },
    /*---------------------------------------*\
	    Format p tags on return: key 13
	\*---------------------------------------*/
    formatPTags: function(elem) {
	    $(elem).off('keypress');
	    $(elem).on('keypress', function(ev){
		    if(ev.keyCode == '13')
		        document.execCommand('formatBlock', false, 'p');
		});
    },
    submitUserInfo: function(){
	    var username = document.getElementById("reglog_username").value;
	    console.log(username)
	    var email = document.getElementById("reglog_email").value;
	    var password = document.getElementById("reglog_password").value;
	    if(username == "" && $document.reglog == 'register'){
	        //navigator.notification.alert("Please enter username", null, "Username Missing", "OK");
	        return;
	    }
	    if(email == ""){
	        //navigator.notification.alert("Please enter password", null, "Password Missing", "OK");
	        return;
	    }
	    if(password == ""){
	        //navigator.notification.alert("Please enter password", null, "Password Missing", "OK");
	        return;
	    }
	    var reglogData = {
		    'email': encodeURIComponent(email),
		    'password': encodeURIComponent(password),
		    'reglog':$document.reglog
	    };

	    if($document.reglog == 'register'){
		    reglogData.username = encodeURIComponent(username);
	    }
	    console.log(reglogData.reglog);
	    $.ajax({
	        url: 'http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/regloguser/',
	        method: 'GET',
	        data: reglogData,
	        crossDomain: true,
	        //dataType: 'json',
	        //contentType: 'application/json',
	        //beforeSend: function ( xhr ) {
	        //    xhr.setRequestHeader( 'Authorization', 'Basic username:password' );
	        //},
	        success: function( data ) {
		        console.log(data);
		        myID = data;
		        localStorage.setItem("myID", myID);
		        switch($document.reglog){
					case 'register':
						$document.showRegistrationGenres();
						console.log('registering');
					break;
					case 'login':
						$('#onboarding_screen').removeClass('active');
						$document.getMyStories();
					break;
				}
	        },
	        error: function( error ) {
	            console.log( error );
	        }
	    });
    },
	showRegistrationGenres: function() {
		var genresul = document.createElement('ul');
		$('#genre_select_list').html(genresul);
		console.log($document.allGenres);
		$.each($document.allGenres, function(g, genre){
		    var genreli = document.createElement('li');
				genreli.innerHTML = '<p>'+genre.name+'</p>';
				genreli.setAttribute('id', genre.term_id);
				genreli.onclick = function () {
		        	$(this).toggleClass('active');
				};
		    $('#genre_select_list ul').append(genreli);
		});
		//$('#setup_user').addClass('active');
		$('#register_submit_genres').on('click', function(){
			var userGenres = [];
			$('#genre_select_list li.active').each(function(){
				userGenres.push($(this).attr('id'));
			});
			$document.submitUserGenres(userGenres);

			return false;
		});
	},
	submitUserGenres: function(userGenres) {
		$.ajax({
	        url: 'http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/submitusergenres/',
	        method: 'GET',
	        data: {'genres': JSON.stringify(userGenres), 'userid':encodeURIComponent(myID) },
	        crossDomain: true,
	        dataType: 'json',
	        contentType: 'application/json',
	        //beforeSend: function ( xhr ) {
	        //    xhr.setRequestHeader( 'Authorization', 'Basic username:password' );
	        //},
	        success: function( data ) {
	            console.log( data );
	            if(data == 1){
		            $('#onboarding_screen').removeClass('active');
		            $document.setupMaps();
	            }
	        },
	        error: function( error ) {
	            console.log( error );
	        }
	    });
	}
};

app.initialize();


function get_my_user_info(){
	$.ajax({
        url: 'http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/getmyuserinfo/'+myID+'/?lat=40.6744073&lng=-73.943335',
        method: 'GET',
        crossDomain: true,
        dataType: 'json',
        contentType: 'application/json',
        //beforeSend: function ( xhr ) {
        //    xhr.setRequestHeader( 'Authorization', 'Basic username:password' );
        //},
        success: function( data ) {
            $('#edit_profile_content').html('<h2>'+data.username+'</h2>');
            if(data.genres){
		        var genreul = document.createElement('ul');
		        	genreul.setAttribute('id', 'my_genres_list');
		        $('#edit_profile_content').append(genreul);
		        $.each(data.genres, function(g, genre){
			        var genreli = document.createElement('li');
			        	genreli.setAttribute('genreid', term_id);
						genreli.innerHTML = '<p>'+genre.name+'</p>';
			        $('#my_genres_list').append(genreli);
		        });
	        }
        },
        error: function( error ) {
            console.log( error );
        }
    });
}

function startStoryFunctions(){
	var storyObj;
	var startStoryLocs = [];
	var genreArray = [];

	$('#choose_genre_button').click(function(){
		var storyTitle = document.getElementById("story_title_input").value;
		var chapterTitle = document.getElementById("first_chapter_title_input").value;
	    var storyText = document.getElementById("story_text_input").value;
		if(storyTitle == ""){
	        //navigator.notification.alert("your story needs a title", null, "title missing", "OK");
	        return;
	    }
	    if(storyText == ""){
	        //navigator.notification.alert("write your story", null, "story", "OK");
	        return;
	    }
	    if(chapterTitle == ""){
	        //navigator.notification.alert("write your story", null, "story", "OK");
	        return;
	    }
	    storyObj = new Object();
    	storyObj.stitle = storyTitle;
    	storyObj.ctitle = chapterTitle;
    	storyObj.ctext = storyText;
    	storyObj.user = myID;

	    var genreul = document.createElement('ul');
        $('#story_genres_list').html(genreul);
        $('#story_genres_screen').addClass('active');
        $('#start_story_screen').addClass('hidden');

		$.each(allGenres, function(i, genre){
			var genreli = document.createElement('li');
				genreli.innerHTML = '<h3>'+genre.name+'</h3>';
				genreli.onclick = function () {
					var selectedGenre = $.inArray(genre.slug, genreArray);
					if (selectedGenre == -1) {
					  genreArray.push(genre.slug);
					} else {
					  genreArray.splice(selectedGenre, 1);
					}
				};
	        $('#story_genres_list ul').append(genreli);
		});
	});

	$('#place_story_locations').on('click', function(){ // Create and display map with current location.
		$('#place_locations_map').addClass('active');
		$('#place_locations_map_content').html('<div id="locations_map"></div>');

		var position = new google.maps.LatLng(currentLoc[0], currentLoc[1]);
		startStoryLocs.push(position); // Push current location to list of locations
		var options = {
			camera: {
		    	latLng: position // an array of LatLng objects
		  	},
			styles: mapStyles,
			disableDefaultUI: true,
		};
		startStoryMap = new google.maps.Map(document.getElementById("locations_map"), options);
		var bounds = new google.maps.LatLngBounds();

		bounds.extend(position); // add position to bounds to show all pins
		marker = new google.maps.Marker({
	        position: position,
	        map: startStoryMap,
	    });
	    startStoryMap.fitBounds(bounds); //Set bounds of the map to show all markers
		google.maps.event.addListener(startStoryMap, 'click', function(event) {
		   placeMarker(event.latLng);
		   startStoryLocs.push(event.latLng); // Add position of the marker that the person dropped.
		});
		var boundsListener = google.maps.event.addListener((startStoryMap), 'bounds_changed', function(event) {
                                                       this.setZoom(16);
                                                       google.maps.event.removeListener(boundsListener);
                                                       });

		function placeMarker(location) {
		    var storymarker = new google.maps.Marker({
		        position: location,
		        draggable: true,
		        map: startStoryMap
		    });
		    var dragStart = google.maps.event.addListener(storymarker, 'dragstart', function(evt){
				startStoryLocs = $.grep(startStoryLocs, function(e){
				     return e != evt.latLng; // Listen for when the drag starts. Find that pin in the array and remove it because its going to change.
				});
			});
		    var dragEnd = google.maps.event.addListener(storymarker, 'dragend', function(evt){
			    startStoryLocs.push(evt.latLng); // Listen for when the drag stops and add it to the array of story locations
			    console.log(evt.latLng.lat());
			});
		}
	});

	$('#submit_story').on('click', function(){
		var storyLocs = [];
		console.log(startStoryLocs);
 		$.each(startStoryLocs, function(l, loc){
 			storyLocs.push([loc.lat(), loc.lng()]);
		});
		storyObj.genres = JSON.stringify(genreArray);
		storyObj.slocs = JSON.stringify(storyLocs);
		console.log(storyObj);
		submitStory();
	});

	function submitStory(){
	    $.ajax({
	        url: 'http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/submitstory/',
	        method: 'GET',
	        data: {'story': JSON.stringify(storyObj)},
	        crossDomain: true,
	        dataType: 'json',
	        contentType: 'application/json',
	        //beforeSend: function ( xhr ) {
	        //    xhr.setRequestHeader( 'Authorization', 'Basic username:password' );
	        //},
	        success: function( data ) {
	            console.log( data );
	        },
	        error: function( error ) {
	            console.log( error );
	        }
	    });
	}
}

function login(){
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    if(username == ""){
        //navigator.notification.alert("Please enter username", null, "Username Missing", "OK");
        return;
    }
    if(password == ""){
        //navigator.notification.alert("Please enter password", null, "Password Missing", "OK");
        return;
    }
    $.get( "http://fullmetalworkshop.com/openstoryapp/wp-admin/admin-ajax.php?action=login&username=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password)+"&_wpnonce=b192fc4204", function( data ) {
	    if(data == "FALSE"){
            //navigator.notification.alert("Wrong Username and Password", null, "Wrong Creds", "Try Again");
        }else if(data != 'FALSE'){
	        userData = JSON.parse(data);
	        localStorage.setItem("myID", userData.ID);
            get_user_stories();
        }
    });
}

function request_genres(){

}

function submit_user_genres(userGenres){
	console.log(JSON.stringify(userGenres), myID);

}

function add_image(){
	navigator.camera.getPicture(cameraResponse, choseImage, {destinationType: Camera.DestinationType.FILE_URI, targetWidth: 900, targetHeight: 900, sourceType: Camera.PictureSourceType.PHOTOLIBRARY});
}

function cameraResponse(e){ // Crop image
	plugins.crop(function success (e) {
		encodeImageUri(e, function(base64){
			console.log(base64);
			save_image(base64);
		});
	}, function fail () {

	}, e)
}

function save_image(e){
	$.ajax({
        url: 'http://fullmetalworkshop.com/openstoryapp/wp-json/openstory/v2/saveuserimage/',
        method: 'POST',
        data: {
	        'username': 'brandon',
	        'userid': 2,
	        'data': e,
        },
        crossDomain: true,
        //dataType: 'json',
        //contentType: 'application/json',
        //beforeSend: function ( xhr ) {
        //    xhr.setRequestHeader( 'Authorization', 'Basic username:password' );
        //},
        success: function( data ) {
	        console.log(data);
            $('#register_user_image').css('background-image', 'url("'+data+'")');
        },
        error: function( error ) {
            console.log( error );
        }
    });
}

encodeImageUri = function(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function() {
        c.width = this.width;
        c.height = this.height;
        ctx.drawImage(img, 0, 0);

        if(typeof callback === 'function'){
            var dataURL = c.toDataURL("image/jpeg");
            callback(dataURL.slice(22, dataURL.length));
        }
    };
    img.src = imageUri;
}

function choseImage(e){

}



//TODO Add 'likes' functionality
//TODO Finish login/register process
//TODO Finish profile edit functionality
//TODO Add image to new stories and daily stories.
